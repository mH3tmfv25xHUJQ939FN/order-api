﻿using AutoMapper;
using MMTOrderAPI.Controllers.Resources;
using System.Collections.Generic;
using System.Linq;

namespace MMTOrderAPI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Internal Resource to API Resource
            CreateMap<List<LastOrdersResource>, CustomerOrderResource>()
                .ForMember(cor => cor.Customer, opt => opt.Ignore())
               .ForMember(cor => cor.Order, opt => opt.MapFrom<CustomerOrderResourceResolver>());

            CreateMap<List<LastOrdersResource>, OrderResource>()
                .ConvertUsing<OrderResourceConvertor>();

            CreateMap<CustomerResource, CustomerOrderResource>()
                .ConvertUsing<CustomerOrderConvertor>();

            //Domain to API Resource

            // API Resource to Domain
        }
    }

    public class CustomerOrderConvertor : ITypeConverter<CustomerResource, CustomerOrderResource>
    {
        public CustomerOrderResource Convert(CustomerResource source, CustomerOrderResource destination, ResolutionContext context)
        {
            destination.Customer.Firstname = source.Firstname;
            destination.Customer.Lastname = source.Lastname;
            destination.Order.DeliveryAddress = $"{source.HouseNumber}, {source.Street}, {source.Town}, {source.Postcode}".Trim();
            if (destination.Order.DeliveryAddress.StartsWith(","))
                destination.Order.DeliveryAddress.TrimStart(',');

            return destination;
        }
    }

    public class CustomerOrderResourceResolver : IValueResolver<List<LastOrdersResource>, CustomerOrderResource, OrderResource>
    {
        public OrderResource Resolve(List<LastOrdersResource> source, CustomerOrderResource destination, OrderResource member, ResolutionContext context)
        {
            member.OrderDate = source.FirstOrDefault()?.ORDERDATE.Date ?? default;
            member.OrderNumber = source.FirstOrDefault()?.ORDERID ?? default;
            member.DeliveryExpected = source.FirstOrDefault()?.DELIVERYEXPECTED.Date ?? default;
            member.ContainsGift = source.FirstOrDefault()?.CONTAINSGIFT ?? default;
            foreach (var lor in source)
            {
                member.OrderItems.Add(new OrderItemResource { Price = lor.PRICE, ProductName = lor.PRODUCTNAME, Quantity = lor.QUANTITY });
            }

            return member;
        }
    }

    public class OrderResourceConvertor : ITypeConverter<List<LastOrdersResource>, OrderResource>
    {
        public OrderResource Convert(List<LastOrdersResource> source, OrderResource destination, ResolutionContext context)
        {
            destination.OrderDate = source.FirstOrDefault()?.ORDERDATE.Date ?? default;
            destination.OrderNumber = source.FirstOrDefault()?.ORDERID ?? default;
            destination.DeliveryExpected = source.FirstOrDefault()?.DELIVERYEXPECTED.Date ?? default;
            destination.ContainsGift = source.FirstOrDefault()?.CONTAINSGIFT ?? default;
            foreach (var lor in source)
            {
                destination.OrderItems.Add(new OrderItemResource { Price = lor.PRICE, ProductName = lor.PRODUCTNAME, Quantity = lor.QUANTITY });
            }

            return destination;
        }
    }
}