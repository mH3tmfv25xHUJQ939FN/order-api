﻿using Newtonsoft.Json.Converters;

namespace MMTOrderAPI.Mapping
{
    internal class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "dd-MMM-yyyy";
        }
    }
}