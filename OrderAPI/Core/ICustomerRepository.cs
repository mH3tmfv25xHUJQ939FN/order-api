﻿using MMTOrderAPI.Controllers.Resources;
using System.Threading.Tasks;

namespace MMTOrderAPI.Core
{
    public interface ICustomerRepository
    {
        Task<CustomerResource> GetCustomerAsync(string email);
    }
}