﻿using MMTOrderAPI.Core.Models;
using System.Collections.Generic;

namespace MMTOrderAPI.Core
{
    public interface IProductRepository
    {
        List<Product> GeProductsByid(List<int> productsId);
    }
}