﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMTOrderAPI.Core.Models
{
    public class OrderItem
    {
        [Key]
        public int OrderItemID { get; set; }

        [Required]
        [ForeignKey("Order")]
        public Order Order { get; set; }

        public int OrderId { get; set; }

        [Required]
        [ForeignKey("Product")]
        public Product Product { get; set; }

        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal? Price { get; set; }
        public bool? Returnable { get; set; }
    }
}