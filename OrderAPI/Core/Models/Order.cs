﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMTOrderAPI.Core.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }

        [ForeignKey("Customer")]
        public Customer Customer { get; set; }

        [Required]
        [StringLength(10)]
        public string CustomerId { get; set; }

        public DateTime OrderDate { get; set; }
        public DateTime DeliveryExpected { get; set; }
        public bool ContainsGift { get; set; }

        [StringLength(30)]
        public string ShippingMode { get; set; }

        [StringLength(30)]
        public string OrderSource { get; set; }

        public IEnumerable<OrderItem> Items { get; set; }

        public Order()
        {
            Items = new List<OrderItem>();
        }
    }
}