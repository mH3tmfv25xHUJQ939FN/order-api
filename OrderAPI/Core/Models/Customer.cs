﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MMTOrderAPI.Core.Models
{
    public class Customer
    {
        [Key]
        [Required]
        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Website { get; set; }
        public DateTime LastLoggedIn { get; set; }
        public string HouseNumber { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string PreferredLanguage { get; set; }
        public IEnumerable<Order> Orders { get; set; }

        public Customer()
        {
            Orders = new List<Order>();
        }
    }
}