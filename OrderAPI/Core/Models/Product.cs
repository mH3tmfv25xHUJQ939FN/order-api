﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MMTOrderAPI.Core.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [StringLength(30)]
        public string ProductName { get; set; }

        public decimal PackHeight { get; set; }
        public decimal PackWidth { get; set; }
        public decimal PackWeight { get; set; }
        public string Colour { get; set; }

        [StringLength(20)]
        public string Size { get; set; }

        public IEnumerable<OrderItem> OrderItems { get; set; }

        public Product()
        {
            OrderItems = new List<OrderItem>();
        }
    }
}