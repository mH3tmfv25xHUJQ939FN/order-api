﻿using MMTOrderAPI.Controllers.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMTOrderAPI.Core
{
    public interface IOrderRepository
    {
        Task<List<LastOrdersResource>> GetOrderByCustomerAsync(string customerId);
    }
}