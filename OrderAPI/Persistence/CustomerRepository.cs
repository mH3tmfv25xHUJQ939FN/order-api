﻿using Microsoft.Extensions.Logging;
using MMTOrderAPI.Controllers.Resources;
using MMTOrderAPI.Core;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Threading.Tasks;

namespace MMTOrderAPI.Persistence
{
    public class CustomerRepository : ICustomerRepository
    {
        private const string APIURL = "API URL";
        private const string APIKEY = "API Key";

        private RestClient _client;
        private readonly ILogger<CustomerRepository> _logger;

        public CustomerRepository(ILogger<CustomerRepository> logger)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            _client = new RestClient(APIURL);
            _logger = logger;
        }

        public async Task<CustomerResource> GetCustomerAsync(string email)
        {
            var restReq = new RestRequest("/api/GetUserDetails", Method.GET);
            restReq.AddParameter("code", APIKEY, ParameterType.QueryString);
            restReq.AddParameter("email", email, ParameterType.QueryString);

            var restResp = await _client.ExecuteAsync(restReq);
            return JsonConvert.DeserializeObject<CustomerResource>(restResp.Content);
        }
    }
}