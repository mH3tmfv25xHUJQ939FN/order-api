﻿using Microsoft.EntityFrameworkCore;
using MMTOrderAPI.Controllers.Resources;
using MMTOrderAPI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMTOrderAPI.Persistence
{
    public class OrderRepository : IOrderRepository
    {
        private readonly SSEDbContext _context;

        public OrderRepository(SSEDbContext context)
        {
            _context = context;
        }

        public async Task<List<LastOrdersResource>> GetOrderByCustomerAsync(string customerId)
        {
            var result = _context.Orders
            .Join(
               _context.OrderItems,
                order => order.OrderId,
                orderItemn => orderItemn.OrderId,
                (order, orderItemn) => new { orderItemn, order }
               )
            .Join(
                _context.Products,
                oi => oi.orderItemn.ProductId,
                product => product.ProductId,
                (oi, product) => new LastOrdersResource
                {
                    PRODUCTNAME = product.ProductName,
                    ORDERID = oi.order.OrderId,
                    ORDERDATE = oi.order.OrderDate,
                    DELIVERYEXPECTED = oi.order.DeliveryExpected,
                    PRICE = oi.orderItemn.Price,
                    QUANTITY = oi.orderItemn.Quantity,
                    CUSTOMERID = oi.order.CustomerId,
                    CONTAINSGIFT = oi.order.ContainsGift
                }
            ).OrderByDescending(rp => rp.ORDERDATE)
            .Where(rp => rp.CUSTOMERID == customerId);

            DateTime mostRecentDate = result.Max(l => l.ORDERDATE);
            return await result.Where(l => l.ORDERDATE == mostRecentDate).ToListAsync();
        }
    }
}