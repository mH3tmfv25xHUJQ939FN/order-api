﻿using Microsoft.EntityFrameworkCore;
using MMTOrderAPI.Core.Models;

namespace MMTOrderAPI.Persistence
{
    public class SSEDbContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        public SSEDbContext(DbContextOptions<SSEDbContext> options)
          : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderItem>().HasKey(oi =>
              new { oi.OrderId, oi.ProductId });

            modelBuilder.Entity<Order>().Ignore(o => o.Customer);
            modelBuilder.Entity<Product>().Ignore(p => p.OrderItems);
            modelBuilder.Entity<OrderItem>().Ignore(o => o.Order);
            modelBuilder.Entity<OrderItem>().Ignore(o => o.Product);
        }
    }
}