﻿using MMTOrderAPI.Core;
using MMTOrderAPI.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace MMTOrderAPI.Persistence
{
    public class ProductRepository : IProductRepository
    {
        private readonly SSEDbContext _context;

        public ProductRepository(SSEDbContext context)
        {
            _context = context;
        }

        public List<Product> GeProductsByid(List<int> productsId)
        {
            return _context.Products.Where(p => productsId.Contains(p.ProductId)).ToList();
        }
    }
}