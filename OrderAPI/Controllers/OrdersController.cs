﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMTOrderAPI.Controllers.Resources;
using MMTOrderAPI.Core;
using System.Threading.Tasks;

namespace MMTOrderAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]
    public class OrdersController : ControllerBase
    {
        private readonly ILogger<OrdersController> _logger;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IProductRepository _productRepository;

        public OrdersController(ILogger<OrdersController> logger, IMapper mapper, IOrderRepository orderRepository, ICustomerRepository customerRepository, IProductRepository productRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _productRepository = productRepository;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> GetLatestOrder([FromBody] CustomerIdentityResource identityResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customerResource = await _customerRepository.GetCustomerAsync(identityResource.CustomerEmail);

            if (customerResource == null)
                return NotFound();

            if (customerResource.CustomerId != identityResource.CustomerId)
                return BadRequest("User's email address does not match the customer number");

            var recentOrderInfo = await _orderRepository.GetOrderByCustomerAsync(identityResource.CustomerId);

            var customerOrderResource = new CustomerOrderResource();

            if (recentOrderInfo == null)
            {
                customerOrderResource.Customer.Firstname = customerResource.Firstname;
                customerOrderResource.Customer.Firstname = customerResource.Lastname;

                return Ok(customerOrderResource);
            }

            _mapper.Map(customerResource, customerOrderResource);
            _mapper.Map(recentOrderInfo, customerOrderResource.Order);

            if (customerOrderResource.Order.ContainsGift)
                customerOrderResource.Order.OrderItems.ForEach(cor => cor.ProductName = "Gift");

            return Ok(customerOrderResource);
        }
    }
}