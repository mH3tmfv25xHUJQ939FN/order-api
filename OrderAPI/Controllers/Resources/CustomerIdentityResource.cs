﻿using Newtonsoft.Json;

namespace MMTOrderAPI.Controllers.Resources
{
    public class CustomerIdentityResource
    {
        public string CustomerId { get; set; }

        [JsonProperty("user")]
        public string CustomerEmail { get; set; }
    }
}