﻿using System;

namespace MMTOrderAPI.Controllers.Resources
{
    public class LastOrdersResource
    {
        public string PRODUCTNAME { get; set; }
        public int ORDERID { get; set; }
        public DateTime ORDERDATE { get; set; }
        public DateTime DELIVERYEXPECTED { get; set; }
        public decimal? PRICE { get; set; }
        public int QUANTITY { get; set; }
        public string CUSTOMERID { get; set; }
        public bool CONTAINSGIFT { get; set; }
    }
}