﻿using MMTOrderAPI.Mapping;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MMTOrderAPI.Controllers.Resources
{
    public class OrderResource
    {
        public int OrderNumber { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime OrderDate { get; set; }

        public string DeliveryAddress { get; set; }

        public List<OrderItemResource> OrderItems { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DeliveryExpected { get; set; }

        [JsonIgnore]
        public bool ContainsGift { get; set; }

        public OrderResource()
        {
            OrderItems = new List<OrderItemResource>();
        }
    }
}