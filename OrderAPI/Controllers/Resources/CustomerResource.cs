﻿using Newtonsoft.Json;
using System;

namespace MMTOrderAPI.Controllers.Resources
{
    public class CustomerResource : CustomerBaseResource
    {
        [JsonProperty("customerId")]
        public string CustomerId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("website")]
        public bool Website { get; set; }

        [JsonProperty("lastLoggedIn")]
        public DateTime LastLoggedIn { get; set; }

        [JsonProperty("houseNumber")]
        public string HouseNumber { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; }

        [JsonProperty("postcode")]
        public string Postcode { get; set; }

        [JsonProperty("preferredLanguage")]
        public string PreferredLanguage { get; set; }
    }
}