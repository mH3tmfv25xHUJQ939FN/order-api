﻿namespace MMTOrderAPI.Controllers.Resources
{
    public class CustomerOrderResource
    {
        public CustomerBaseResource Customer { get; set; }
        public OrderResource Order { get; set; }

        public CustomerOrderResource()
        {
            Order = new OrderResource();
            Customer = new CustomerBaseResource();
        }
    }
}