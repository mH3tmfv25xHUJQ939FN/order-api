﻿using Newtonsoft.Json;

namespace MMTOrderAPI.Controllers.Resources
{
    public class CustomerBaseResource
    {
        [JsonProperty("firstName")]
        public string Firstname { get; set; }

        [JsonProperty("lastName")]
        public string Lastname { get; set; }
    }
}