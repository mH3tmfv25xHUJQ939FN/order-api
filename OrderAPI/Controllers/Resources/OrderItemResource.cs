﻿using Newtonsoft.Json;

namespace MMTOrderAPI.Controllers.Resources
{
    public class OrderItemResource
    {
        [JsonProperty("Product")]
        public string ProductName { get; set; }

        public int Quantity { get; set; }

        [JsonProperty("priceEach")]
        public decimal? Price { get; set; }
    }
}