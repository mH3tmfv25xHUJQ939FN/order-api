# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The is a text project for an assignment
* Version 1.0.0.0

### How do I get set up? ###

* Add the SSEConnectionString attribute to ConnectionStrings section of appsettings.Development.json file for the development environment and appsettings.json for production
* Add the proper API Url and API key to the CustomerRepository.cs in the Persistence namespace/folder
